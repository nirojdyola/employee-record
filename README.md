## Backend

In the backend directory, copy or move .example.env to .env

Configure port number and database settings in .env file.

Run

To run migration and seeders: `npm run db`

To run dependencies: `npm install`

To run the server: `npm run dev`

Please check username in the [seeder file](https://gitlab.com/nirojdyola/employee-record/-/blob/master/backend-node/db/seeders/20201125035447-users-seeders.js).