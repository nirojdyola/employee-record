const en = {
  /*login Credential*/
  login: 'Login',
  log_in: 'Log In',
  password: 'Password',
  forgot_your_password: 'Forgot your password',
  email: 'Email',
  your_email_address: 'Your Email Address',
  remember_me: 'Remember Me',
  forgot_password: 'Forgot Password?',
  forgot_password1: 'Forgot Password',
  back_to_login: 'Back to Login',
  password_reset: 'Request Password Reset',
  login_message:
    'Enter your credentials to login into Clausen Business Solitions',
  forgot_message: 'Please enter the email you use to sign in to your account.',
  /*button*/
  save_now: 'Save Now',
  apply_filter: 'Apply Filter',
  reset_filter: 'Reset Filter',
  cancel: 'Cancel',
  clear: 'Clear',
  reply: 'Reply',
  comment: 'Comment',
  save_and_create_new_ticket: 'Save and Create New Ticket',
  confirm_and_close: 'Confirm and Close',
  confirm_and_transfer_another_items: 'Confirm and Transfer Another Items',
  edit_article: 'Edit Article',
  /*Module*/
  user_management: 'User Management',
  chat: 'Chat',
  ticket: 'Ticket',
  crm: 'CRM',
  contact_center: 'Contact Center',
  feedback: 'Feedback',
  mms: 'MMS',
  dashboard: 'Dashboard',
  /*Splash Screen*/
  choose_subsystem: 'Choose Subsystem',
  /*User Management*/
  user: 'User',
  user_list: 'User List',
  user_rights: 'User Rights',
  user_roles: 'User Roles',
  user_name: 'User Name',
  user_profile: 'User Profile',
  role: 'Role',
  add_additional_role: 'Add Additional Role',
  add_role: 'Add Role',
  role_setup: 'Role Setup',
  system: 'System',
  role_name: 'Role Name',
  privillage: 'Privillage',
  add_user: 'Add User',
  all_users: 'All Users',
  add_new_user: 'Add New User',
  add_new_role: 'Add New Role',
  edit_user: 'Edit User',
  delete_user: 'Delete User',
  save_user: 'Save User',
  user_deatils: 'User Details',
  upload_image: 'Upload Image',
  assigned_tickets: 'Assigned Tickets',
  action: 'Action',
  first_name: 'First Name',
  last_name: 'Last Name',
  email_address: 'Email Address',
  contact_info: 'Contact Info',
  address: 'Address',
  view: 'View',
  add: 'Add',
  edit: 'Edit',
  delete: 'Delete',
  search: 'Search',

  /*Chat System*/

  closed_chat: 'Closed Chat',
  closed_chat_details: 'Closed Chat Details',
  archived_chat: 'Archived Chat',
  archived_chat_details: 'Archived Chat Details',
  chat_metrics: 'Chat Metrics',
  id: 'ID',
  customer_name: 'Customer Name',
  employee_name: 'Employee Name',
  closed_at: 'Closed At',
  last_message: 'Last Message',
  actions: 'Action',
  no_of_chat: 'No. of Chat',
  no_of_message: 'No. of Message',
  no_messages_received: 'No. Messages Received',
  no_messages_send: 'No. Messages Send',
  time_spent_in_call: 'Time Spent In Call',
  role_descripition: 'Role Descripition',
  name: 'Name',
  assignee: 'Assignee',
  description: 'Description',
  transfer_chat: 'Transfer Chat',
  select_user: 'Select User',
  transfer_modal_closed_message: 'Are you sure you want closed this ?',
  transfer_modal_archived_message: 'Are you sure you want archived this ?',
  yes: 'Yes',
  no: 'No',
  miss_called: 'Your missed the call.',
  active: 'Active',
  in_active: 'Inactive',
  select_appartment: 'Select Appartment',
  select_assignee: 'Select Assignee',
  select_customer: 'Select Customer',
  select_cost_recepient: 'Select Cost Recepient',
  select_house_owner: 'Select House Owner',
  select_priority: 'Select Priority',
  write_description: 'Write Description',
  call_ended: 'Call ended',

  /*Ticket Board*/
  ticket_board: 'Ticket Board',
  add_task: 'Add Task',
  open: 'Open',
  inprogress: 'In Progress',
  done: 'Done',
  closed: 'Closed',
  recursive_task: 'Recursive Task',
  normal_task: 'Normal Task',
  create_ticket: 'Create Ticket',
  ticket_details: 'Ticket Details',
  title: 'Title',
  ticket_title: 'Ticket Title',
  object: 'Object',
  customer: 'Customer',
  cost_recipient: 'Cost Recepient',
  house_owner: 'House Owner',
  priority_label: 'Priority Label',
  priority: 'Priority',
  upload_files: 'Upload Files',
  descriptions: 'Descriptions',
  save_create_new_ticket: 'Save and Create New Ticket',
  author: 'Author',
  created_by: 'Created By',
  created_at: 'Created At',
  last_changed_by: 'Last Change By',
  last_changed_at: 'Last Change At',
  status: 'Status',
  ticket_log: 'Ticket Log',
  ticket_list: 'Ticket List',
  recursive_interval: 'Recursive Interval',
  recursive_untill: 'Recursive Untill',
  recursive_start: 'Recursive Start',
  appartment: 'Appartment',
  ticket_created: 'Ticket has been created.',
  ticket_error: 'Ticket has not been created.',
  expand: 'Expand',
  ticket_list_error: 'There was an error processing your request.',
  select_recursive_interval: 'Select Recursive Interval',
  title_required: 'Title is required.',
  repeating_info: 'Repeating Info',
  interval: 'Interval',
  repeat_from: 'Repeat From',
  repeat_until: 'Repeat Until',
  validate_recursive_date: 'Please enter valid recursive date.',
  comment_here: 'Comment Here...',
  comment_required: 'Comment is required.',
  id_not_found: 'ID not found.',
  edit_ticket: 'Edit Ticket',
  ticket_updated: 'Ticket updated successfully',
  change_status: 'Change Status',
  status_updated: 'Status updated successfully',
  select_status: 'Select Status',
  comment_error: 'Comment error',

  /*CRM*/
  contact_list: 'Contact List',
  add_contact: 'Add Contact',
  label: 'Label',
  source: 'Source',
  destination: 'Destination',
  added: 'Added',
  last_activities: 'Last Activities',
  last_ticket: 'Last Ticket',
  street: 'Street',
  house_no: 'House No.',
  post_code: 'Post Code',
  town: 'Town',
  country: 'Country',
  phone: 'Phone No.',
  mobile: 'Mobile No.',
  company: 'Company',
  fellow_travelers: 'Fellow Travelers',
  pets: 'Pets',
  equipment_features: 'Equipment Features',
  services: 'Services',
  notes_external: 'Notes External',
  notes_internal: 'Notes Internal',
  add_now: 'Add Now',
  edit_contact: 'Edit Contact',
  event_history: 'Event History',
  chanel: 'Chanel',
  date_time: 'Date/Time',
  contact_details: 'Contact Details',

  /*Inventory*/
  inventory: 'Inventory',
  inventory_name: 'Inventory Name',
  used_item: 'Used Item',
  quantity: 'Quantity',
  total_quantity: 'Total Quantity',
  from: 'From',
  to: 'To',
  add_item: 'Add Item',

  /*MMS*/
  transfer_item: 'Transfer Items',
  select_articles: 'Select Articles',
  search_article: 'Search Article',
  units: 'Units',
  unit: 'Unit',
  unit_details: 'Unit Details',
  select_source_warehouse: 'Select Source Warehouse',
  rooms: 'Rooms',
  room: 'Room',
  add_room: 'Add Room',
  room_name: 'Room Name',
  racks: 'Racks',
  rack: 'Rack',
  shelf: 'Shelf',
  shelves: 'Shelves',
  select_destination_warehouse: 'Select Destination Warehouse',
  warehouse: 'Warehouse',
  warehouses: 'Warehouses',
  add_warehouse: 'Add Warehouse',
  warehouse_information: 'Warehouse Information',
  articles: 'Articles',
  article: 'Article',
  add_article: 'Add Article',
  articles_names: 'Articles Names',
  type: 'Type',
  types: 'Types',
  type_details: 'Type Details',
  buying_price: 'Buying Price',
  selling_price: 'Selling Price',
  seller: 'Seller',
  seller_home_page_shop: 'Seller Home Page/Shop',
  maximum_stock: 'Maximum Stock',
  minimum_stock_notification: 'Minimum Stock Noification',
  ean_number: 'EAN Number',
  notes: 'Notes',
  article_details: 'Article Details',
  objects: 'Object',
  object_id: 'Object ID',
  object_name: 'Object Name',
  booking: 'Booking',
  tickets: 'Tickets',
  inventory_load: 'Invenory Load',
  object_details: 'Object Details',
  ticket_open_inprogress_done: 'Ticket (Open / Inprogress / Done)',
  abbreviation: 'Abbreviation',
  street_number: 'Street Number',
  is_active: 'Is Active',
  type_name: 'Type Name',
  add_type: 'Add Type',
  unit_name: 'Unit Name',
  add_unit: 'Add Unit',
  transfor_log: 'Transfer Log',
  ticket_source: 'Ticket Source',
  done_by: 'Done By',
  required: 'required',
  user_created: 'User has been created',
  user_creation_failed: 'User creation failed',
  user_not_found: 'User not found',
  update: 'Update',
  user_updated: 'User updated successfully',
  roles_not_found: 'Roles not found',
};

export default en;
