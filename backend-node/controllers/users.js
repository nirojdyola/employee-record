const { User, Role } = require("./../db/models");
const validator = require("validator");
const authService = require("./../services/auth.services");

module.exports = {
  /**
   * Login User
   */
  async login(req, res, next) {
    let result = {};
    let errors = [];

    if (!req.is("application/json")) {
      errors.push({ msg: "Expects 'application/json'" });
      return res.status(200).send(errors);
    }

    /** username and Password Validation */
    if (validator.isEmpty(req.body.username)) {
      errors.push({ msg: "Please enter a valid username." });
    }

    if (validator.isEmpty(req.body.password)) {
      errors.push({ msg: "Please enter a password." });
    }

    if (errors.length > 0) {
      result.response = "error";
      result.errors = errors;
      return res.status(200).send(result);
    }

    try {
      await authService.authUser(req.body, (err, data) => {
        if (err) {
          errors.push({ msg: err });
          result.response = "errors";
          result.errors = errors;
          return res.status(200).send(result);
        }
        // no user
        if (data.response === "errors") {
          errors.push({ msg: "Please check username/password" });
          result.response = "errors";
          result.errors = errors;
          return res.status(200).send(result);
        }
        res.status(200).send(data);
      });
    } catch (err) {
      next(err);
    }
  },
};
