let chai = require("chai");
let chaiHttp = require("chai-http");
const { response } = require("../app");
let server = require("../app");
const token = process.env.BEARER_TOKEN;
chai.should();
console.log("======Token======", token);
chai.use(chaiHttp);

describe("Employee API", () => {
  /**
   * List the Emplpoyee Details
   */
  describe("GET /api/employees", () => {
    it("it should GET all the employee details", (done) => {
      chai
        .request(server)
        .get("/api/employees")
        .set({
          Authorization: `Bearer ${token}`,
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("response").eq("success");
          done();
        });
    });

    it("it should not GET all the employee details", (done) => {
      chai
        .request(server)
        .get("/api/employee")
        .set({
          Authorization: `Bearer ${token}`,
        })
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  /**
   * Get Employee Detail by ID
   */
  describe("GET /api/employees/:id?", () => {
    it("it should GET employee detail by ID", (done) => {
      const id = 1;
      chai
        .request(server)
        .get("/api/employees/" + 1)
        .set({
          Authorization: `Bearer ${token}`,
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property("response").eq("success");
          done();
        });
    });
  });

  /**
   * Insert Employee Detail
   */
  describe("POST /api/employees/addOREdit", () => {
    it("it should POST employee detail", (done) => {
      const data = {
        fullname: "Niroj",
        dob: "2020-02-02",
        gender: 1,
        salary: 10,
        designation: "Senior Developer",
        createdBy: 1,
        updatedBy: 1,
      };
      chai
        .request(server)
        .post("/api/employees/addOREdit")
        .set({
          Authorization: `Bearer ${token}`,
        })
        .send(data)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });

    it("it should not POST employee detail if empty fullname", (done) => {
      const data = {
        fullname: "",
        dob: "2020-02-02",
        gender: 1,
        salary: 10,
        designation: "Senior Developer",
        createdBy: 1,
        updatedBy: 1,
      };
      chai
        .request(server)
        .post("/api/employees/addOREdit")
        .set({
          Authorization: `Bearer ${token}`,
        })
        .send(data)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("response").eq("error");
          done();
        });
    });
  });
});
