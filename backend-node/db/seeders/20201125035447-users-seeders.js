"use strict";
const User = require("../models").User;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let data = [
      { username: "niroj", password: "1Password*", status: 1 },
      { username: "demo", password: "1Password*", status: 1 },
    ];
    return await User.bulkCreate(data);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("users", null, {});
  },
};
